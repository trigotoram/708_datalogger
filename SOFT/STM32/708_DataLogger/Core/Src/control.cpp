//for debug in RAM:
// 1. add variable VECT_TAB_SRAM
// 2. change *.ld from FLASH to RAM file
// 3. #define VECT_TAB_OFFSET  0x00020000  - set to 0x00

#include "control.h"
#include "board.h"
#include "hal.h"
#include "_crc.h"
#include "_fifo.h"
#include "_misc.h"
#include "_debug.h"
#include "_delay.h"
#include "xprintf.h"
#include <string.h>
//#include "halUSB.h"
#include "colors.h"
#include "halPaint.h"
#include "modPaint.h"
#include "modKey.h"
#include "halKey.h"
#include "modSysClock.h"
#include "modRTC.h"
RTC_t sys_time;
RTC_cnt_t sys_time_cnt;

//U32 errors = 0;
INT i, size;
S32 s32tmp;
//U8 mode = 0; // work mode
SYSTIME USB_delay;
CHAR str[ 256 ];
U8 tmp_buf[ 4096 ];
U8 tmp8;
U32 tmp32;

#include "modConfig.h"
modConfig_t config;
modConfig_storage_t storage;
#include "modMenu.h"


//#include "modSD.h"
//modSD_t SD;

// internal ================================================================

sys_t sys;

rec_t record;
//U32 cnt_pos_wr = 0;
//U32 cnt_wr = 0;

void modConfig_restore( modConfig_storage_t *storage )
{
	test_param( NULL == storage );

	storage->RTC_correction = 0;
    //config->RTC_calibration = 1;
    //config->timezone = +3; //in hours

	storage->screen_contrast = 255;
	storage->screen_inv_on = FALSE;
	storage->screen_inv_period = ( 10 * 1000 ); // 10 Seconds

	storage->timesave_period = ( 60 * 60 * 1000 ); //1 Hour
	storage->timesave_change_period = ( 1 * 1000 ); //1 sec

	//menu_timesave_period_set(1);
	//storage->record_size_max = 10;

	storage->VDDA_V = 3300; //in mV
	storage->ADC_VBAT_low = 2000; //in mV
	storage->ADC_VBAT_offset = 0; //in mV
	storage->ADC_VBAT_hysteresis = 100; //in mV
	storage->ADC_PWR_divider = 4900; //external res divider
	storage->ADC_PWR_low = 6000; //in mV
	storage->ADC_PWR_high = 15000; //in mV
	storage->ADC_PWR_offset = 500; //in mV - diode on power :(
	storage->ADC_PWR_hysteresis = 100; //in mV
	storage->ADC_IN1_divider = 4900;
	storage->ADC_IN1_offset = 0;
	storage->ppm_divider = 2500; // div = 2.5

	// in 0.1C
	storage->temperature_low = -400;
	storage->temperature_high = 1500;
	storage->temperature_offset = 0; //-200 +200C *100
	storage->temperature_hysteresis = 100; //0.1C

	storage->humidity_low = 0;
	storage->humidity_high = 1000;
	storage->humidity_offset = 0;
	storage->humidity_hysteresis = 10; //0.1%

	//storage->INs_f = HALPIN_COUNTER_MAX;
    /* strcpy( config->pass, "passqwerty" ); */

	storage->WDT_on = FALSE;
	storage->dummy = 0;
}


void record_write( void )
{
	U32 record_size = sizeof( rec_t ); //get size of structure
	U8 PWR_state = (sys.PWR_state & 0x02 ) ? 0x80 : 0 ; //INV TODO
	// TODO		PWR_ON

	halRTC_cnt_get( &sys_time_cnt ); //get time

	// fill structure...
	record.UNIX_TIME = sys_time_cnt;
	record.INs = ( sys.pins | PWR_state ); // add for 16 bytes record
	record.ppm = sys.ppm;
	record.temperature = sys.temperature;
	record.humidity = (U8)(sys.humidity / 10);
	record.crc = CRC16_MODBUS( &record, record_size - sizeof( record.crc ), 0xFFFF );

	//copy to temp buffer
	_memcpy( &tmp_buf[0], &record, record_size );

	//write...
	sys_record_write( &tmp_buf[0], sys.cnt_pos_wr, record_size );
}


#include "fatfs.h"
#include "usb_host.h"
#include "ff_gen_drv.h"

extern ApplicationTypeDef Appli_state;
FATFS USBDISKFatFs;           /* File system object for USB disk logical drive */
FIL USB_file_csv;                   /* File object */
//char USBH_Path[4];           /* USBH logical drive path */
extern USBH_HandleTypeDef hUsbHostFS;

FRESULT fresult;        /* FatFs function common result code */
U32 byteswritten, bytesread;  /* File write/read counts */

void sys_USB_Drive_init( void )
{
	MX_USB_HOST_Init();
	MX_FATFS_Init();
}


void sys_USB_Drive_deinit( void )
{
	/* Init host Library, add supported class and start the library. */
	if (USBH_Stop(&hUsbHostFS) != USBH_OK)
	{
		Error_Handler();
	}
//	if (USBH_RegisterClass(&hUsbHostFS, USBH_MSC_CLASS) != USBH_OK)
//	{
//		Error_Handler();
//	}
	if (USBH_DeInit(&hUsbHostFS) != USBH_OK)
	{
		Error_Handler();
	}
	//MX_FATFS_Init();
	f_mount(NULL, (TCHAR const*)"", 0);
}


void sys_USB_Drive_save( void )
{
	U32 i = 0;
	U32 record_size = sizeof( rec_t );
	U32 records;// = ( REC_NUM_MAX );
	U16 tmp_crc;
	RTC_t time;
	RTC_cnt_t time_cnt;

	LED_TEST_ON;

	//sys_TIM1_stop(); // stop ADC

	paint_screen_clear();
	sys_log_reset();
	sys_log_out(  "USB Drive INIT -OK" );

	PO_I2C_PWR_ON_H; //for WDT TODO

	_delay_ms( 500 ); //for more re

	fresult = f_mount( &USBDISKFatFs, (TCHAR const*)"0", 1 ); //1- Immediate
	if( ( FR_NOT_READY == fresult ) ||
		( FR_DISK_ERR == fresult ) ||
		( FR_INVALID_DRIVE == fresult ))
	{
		LED_FAULT_ON; /* FatFs Initialization Error */
		xsprintf( str, "f_mount ERR!" );
		paint_text_col_row( 0, 1, str );
		paint_screen_update();
	}
	else
	{
		xsprintf( str, "f_mount -OK" );
		paint_text_col_row( 0, 1, str );
		paint_screen_update();

		// get time for FAT
		halRTC_time_get( &sys_time );
		modRTC_time_to_cnt( &sys_time, &sys_time_cnt );

		xsprintf( str, "0:%u%02u%02u-%02u%02u55.csv",
				sys_time.year, sys_time.month, sys_time.day,
				sys_time.hour, sys_time.minute );

		fresult = f_open( &USB_file_csv, str, FA_CREATE_ALWAYS | FA_WRITE );
		if( fresult != FR_OK )
		{
			LED_FAULT_ON;
			xsprintf( str, "f_open ERR!" );
			paint_text_col_row( 0, 2, str );
			paint_screen_update();
		}
		else
		{

			xsprintf( str, "f_write -OK" );
			paint_text_col_row( 0, 2, str );
			paint_screen_update();

			fresult = f_lseek( &USB_file_csv, 0 ); //set
			if( fresult != FR_OK )
			{
				xsprintf( str, "f_lseek ERR!" );
				paint_text_col_row( 0, 3, str );
				paint_screen_update();
			}

			size = xsprintf( str,
				"Time;PWR_ON;IN1;IN2;IN3;IN4;IN5;IN6;ppm;Temperature;Humidity\r\n" );

			fresult = f_write( &USB_file_csv, str, size, (UINT *)&byteswritten);
			if( (byteswritten != (U32)size ) || (fresult != FR_OK) )
			{
				xsprintf( str, "f_write1 ERR!" );
				paint_text_col_row( 0, 4, str );
				paint_screen_update();
			}
			else
			{
				// compute all records num
				records = sys.cnt_all_wr; //( sys_memory_get_size_max() / record_size );
				U32 cnt_pos_wr = sys.cnt_pos_wr;

				for( i = 0; i < records; i++ ) //===============================
				{
					//Compute next
					cnt_pos_wr = cnt_pos_wr - record_size;
					if( cnt_pos_wr > sys_memory_get_size_max() )
					{
						cnt_pos_wr = sys_memory_get_size_max() - record_size;
					}

					// read data
					if( RET_OK != sys_record_read( &tmp_buf[0], cnt_pos_wr, record_size ) )
					{
						break;
					}

					_memcpy( &record, &tmp_buf[0], record_size ); //copy...

					// test CRC
					tmp_crc = CRC16_MODBUS( &record, record_size - sizeof( record.crc ), 0xFFFF );
					if( tmp_crc == record.crc )
					{
						time_cnt = record.UNIX_TIME;
						modRTC_cnt_to_time( &time_cnt, &time );

						size = xsprintf( str, "%04u.%02u.%02u.%02u.%02u.%02u;%u;%u;%u;%u;%u;%u;%u;%d;%d.%u;%u\r\n",
							time.year, time.month, time.day,
							time.hour, time.minute, time.second,
							(record.INs & 0x80) ? 1 : 0,
							(record.INs & 0x01) ? 1 : 0,
							(record.INs & 0x02) ? 1 : 0,
							(record.INs & 0x04) ? 1 : 0,
							(record.INs & 0x08) ? 1 : 0,
							(record.INs & 0x10) ? 1 : 0,
							(record.INs & 0x20) ? 1 : 0,
							record.ppm,
							record.temperature / 10, record.temperature % 10,
							record.humidity );

						// test

						fresult = f_write( &USB_file_csv, str, size, (UINT *)&byteswritten );
						if( (byteswritten != (U32)size ) || (fresult != FR_OK) )
						{
							xsprintf( str, "f_write2 ERR!" );
							paint_text_col_row( 0, 4, str );
							paint_screen_update();
							break;
						}
						else
						{
							// error record
						}
					}
					if( ( i % 10 ) == 0 )
					{
						sys_USB_Drive_paint_progress( records, i );
					}

				}//for(...
			}

		} //f_open == FR_OK

		fresult = f_close( &USB_file_csv ); //close file
		if( fresult != FR_OK )
		{
			xsprintf( str, "f_close ERR!" );
			paint_text_col_row( 0, 4, str );
			paint_screen_update();
		}

		if( i >= records )
		{
			sys_USB_Drive_paint_progress( records + 1, i +1 );
			xsprintf( str, "Completed -OK!" );
			paint_text_col_row( 0, 5, str );
			paint_screen_update();
		}
		else
		{
			sys_USB_Drive_paint_progress( records + 1, i +1 );
			xsprintf( str, "Completed -ERR!" );
			paint_text_col_row( 0, 5, str );
			paint_screen_update();
			_delay_ms( 5000 );
		}
	}


	_delay_ms( 2000 ); //delay for ?
	LED_TEST_OFF;
	//sys_TIM1_start();

}


void sys_USB_Drive_task(void)
{
	MX_USB_HOST_Process();

    if( Appli_state == APPLICATION_START )
	{
    	sys_USB_Drive_save();
		Appli_state = APPLICATION_IDLE;
	}
	else if( Appli_state == APPLICATION_IDLE )
	{
	}
}


void sys_to_default_( void )
{

	//sys.memory_mode = MEMORY_MODE_NONE;
}


void control_run( void )
{
	static SYSTIME mode_delay;
	static SYSTIME timesave_delay;
	static U8 sys_pins_old = 0;

	//sys_to_default();
	sys.BATT_OK = FALSE;
	sys.mode = MODE_INIT;
	sys.cnt_pos_wr = 0;
	sys.cnt_all_wr = 0;
	sys.PWR_state = PWR_OFF;

	sys_init(); //HAL, RTC, etc...

	// menu init ===============================================================
	menu_init( 0 );

    while( TRUE ) // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    {
    	// mode switch
		switch( sys.mode )
		{
		case MODE_INIT:
			// init all HAL
			//sys_to_default();
			sys.mode = MODE_IDLE;
			mode_delay = modSysClock_get();

			break;

		case MODE_IDLE:
			if( TRUE == modSysClock_timeout_ms( &mode_delay, 100 ))
			{
				record_write(); //save - power on TODO
				sys_pins_old = sys.pins; //for often use

				if( sys.PWR_state != PWR_ON )
				{
					sys.mode = MODE_OFF;
				}
				else if( sys.BATT_OK != TRUE )
				{
					paint_screen_clear();

					xsprintf( str, "  Replace battery!  " );
					paint_text_col_row( 0, 2, str );
					xsprintf( str, "Bat. voltage %4u mV", sys.ADC_VBAT_mV );
					paint_text_col_row( 0, 3, str );

					paint_screen_update();
				}
				else
				{
					// USB init ================================================================
				#if USB_NEED
					//halUSB_init();
				#endif
					sys_USB_Drive_init();
					timesave_delay = modSysClock_get();
					sys.mode = MODE_RUN;
				}
			}
			break;

		case MODE_RUN:
			if( sys.PWR_state == PWR_OFF )
			{
				sys.mode = MODE_OFF;
			}
			else
			{
				//
				if( TRUE == modSysClock_timeout_ms( &timesave_delay, storage.timesave_period )) //TODO
				{
					record_write();
				}
				else if( ( modSysClock_get_past_time_ms( mode_delay ) > storage.timesave_change_period ) && // 1 every 1 sec
					(
					( sys.pins != sys_pins_old ) || //pin change
					( sys.ADC_PWR_mV > storage.ADC_PWR_high	) // overvoltage //1 hour
					) )
				{
					mode_delay = modSysClock_get();
					record_write();
					sys_pins_old = sys.pins;
				}

				menu_run( str );
			}

			sys_USB_Drive_task();

			break;

		case MODE_OFF:
			//sys_USB_Drive_deinit();
			record_write(); //save power off
			sys_sleep();
			halMCU_system_reset(); //<-----------------
			mode_delay = modSysClock_get();
			sys.mode = MODE_IDLE;
			//}
			break;

		default: while(1){}; break;
		}

  		modKey_run();

  		sys_INs_run();

  		sys_sensor_run();

  		sys_screen_run();

		//halWDT_reset();

    } //while( TRUE )
}
