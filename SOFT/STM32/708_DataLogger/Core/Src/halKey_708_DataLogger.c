/*
 * @file    
 * @author  Ht3h5793
 * @date    10.03.2014
 * @version V9.0.0
 * @brief  
*/

#include "halKey.h"
#include "board.h"



void halKey_init( void )
{

}


INT halKey_get_pin_state( INT pin )
{
	INT state = 0;
    
    switch( pin )
    {
	case KEY_RIGHT:
		if( KEY_RIGHT_IN == 0 ) //to GND!
		{
			state =  1;
		}
		break;

	case KEY_LEFT:
		if( KEY_LEFT_IN == 0 ) //to GND!
		{
			state =  1;
		}
		break;

	case KEY_DOWN:
		if( KEY_DOWN_IN == 0 ) //to GND!
		{
			state =  1;
		}
		break;

	case KEY_UP:
		if( KEY_UP_IN == 0 ) //to GND!
		{
			state =  1;
		}
		break;

	case KEY_OK:
		if( KEY_OK_IN == 0 ) //to GND!
		{
			state =  1;
		}
		break;

	default: break;
    }

    return state;
}

